import { useState, useEffect, useContext } from 'react'
import { Form, Button} from 'react-bootstrap'
import { Navigate, useNavigate } from 'react-router-dom'
import Swal from 'sweetalert2'
import UserContext from '../UserContext'

export default function Register(){

//do not use null for initial state due to server schema type: null might contradict turn to type: string 

//set isnot a keyword, the state is destrcuturing
	
	const {user} = useContext(UserContext)
  	console.log(user)

  	const history = useNavigate()

  	const [firstName, setFirstName] = useState('')
  	const [lastName, setLastName] = useState('')
  	const [mobileNo, setMobileNo] = useState('')
	const [email, setEmail] = useState('')
	const [password1, setPassword1] = useState('')
	//const [password2, setPassword2] = useState('')
	const [ isActive, setIsActive] = useState(false)

	console.log(email)
	console.log(password1)

	function registerUser(e){
		e.preventDefault()

		fetch('http://localhost:4000/users/checkEmail', {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				email: email
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)

		if(data === true){
			Swal.fire({
				title: "Email Already Exists",
				icon: "error",
				text:"Please provide another email"
			})
		} else {
			fetch('http://localhost:4000/users/register',{
				method: 'POST',
				headers: {
					'Content-Type': 'application/json'
				},
				body: JSON.stringify({
					firstName: firstName,
					lastName: lastName,
					mobileNo: mobileNo,
					email: email,
					password: password1
				})
			})
			.then(res => res.json())
			.then(data => {
				console.log(data)
				if(data !== false){
					console.log(data)
				Swal.fire({
				title: "Email Registered Successfully",
				icon: "success",
				text:"You may proceed to Login"
			})
				setEmail('')
				setPassword1('')
				setFirstName('')
				setLastName('')
				setMobileNo('')
				history("/login")
		}else {
			Swal.fire({
				title: "Something went wrong",
				icon: "error",
				text:"Please contact Admin"
			})
		} 
			})
		}
		})

		
	}

	useEffect(() => {
		if((firstName !== '' && lastName !== '' && mobileNo !== '' && email !== "" && password1 !== "")){
			setIsActive(true)
		} else {
			setIsActive(false)
		}
	}, [email, password1, firstName, lastName, mobileNo])

	/*if (path){

	}*/

	return(
		(user.id !== null)?

		<Navigate to="/courses"/>
		:

		<Form className="border border-secondary p-3 my-3" onSubmit={e => registerUser(e)}>
			<h1 className="text-center">Registration Section</h1>

			<Form.Group controlId="firstName">
			<Form.Label>First Name:</Form.Label>
			<Form.Control type="text" placeholder="Input your first name here" required value={firstName} onChange={e => setFirstName(e.target.value)}/>
			</Form.Group>

			<Form.Group controlId="lastName">
			<Form.Label>Last Name:</Form.Label>
			<Form.Control type="text" placeholder="Input your last name here" required value={lastName} onChange={e => setLastName(e.target.value)}/>
			</Form.Group>

			<Form.Group controlId="mobileNo">
			<Form.Label>Mobile Number:</Form.Label>
			<Form.Control type="text" placeholder="Input your mobile number here" required value={mobileNo} onChange={e => setMobileNo(e.target.value)}/>
			</Form.Group>

			<Form.Group controlId="userEmail">
			<Form.Label>Email Address</Form.Label>
			<Form.Control type="email" placeholder="Enter your email here" required value={email} onChange={e => setEmail(e.target.value)}/>
			<Form.Text className="text-muted"> We will never share your email with anyone else.
			</Form.Text>
			</Form.Group>

			<Form.Group controlId="password1">
			<Form.Label>Password</Form.Label>
			<Form.Control type="password" placeholder="Input your password here" required value={password1} onChange={e => setPassword1(e.target.value)}/>
			</Form.Group>

			

{/*rendering submit button based on isActive*/}
			{ isActive ? 
				<Button variant="primary" type="submit" id="submitBtn" className="my-3">Submit
				</Button>
				:
				<Button variant="danger" type="submit" id="submitBtn" className="my-3" disabled>Submit
				</Button>
			}

		</Form>

		)
}
