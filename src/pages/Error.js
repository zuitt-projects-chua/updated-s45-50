import React, {useState} from 'react'
import {Link} from 'react-router-dom'
import { Button, Container } from 'react-bootstrap'

export default function Error() {

	

	return (
		
			<Container className="p-3">
				<h6>Zuitt Booking</h6>
				<h1>404 Page Not Found</h1>
				<p>It seems the link you are trying to access is not available.</p>
			
				<p>Go back to <Button variant="primary" as={Link} to="/" >Home</Button></p>



			</Container>
		



		)
}
		
