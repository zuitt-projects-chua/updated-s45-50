import { useState, useEffect} from 'react'
//import {Row} from 'react-bootstrap'
import coursesData from "../data/coursesData"
import CourseCard from "../components/CourseCard"

export default function Courses () {
	const [courses, setCourses] = useState([])

	//console.log(coursesData);
	//console.log(coursesData[0]);
	useEffect(() => {
		fetch('http://localhost:4000/courses')
		//fetch('http://localhost:4000/products')
		.then(res => res.json())
		.then(data => {
			console.log(data)
			setCourses(data.map(course => {
	
	return (
		//key used to identify each child
		<CourseCard key={course._id} courseProp={course}/>

	)
	}))
	})
	}, [])

/*const courses = coursesData.map(course => {
	console.log(course)
	return (
		//key used to identify each child
		<CourseCard key={course.id} courseProp={course}/>

	)
})*/

	return (
		<>
			<h1>Courses</h1>
		{/*courseProp parameter can be named anything*/}
			{/*<CourseCard courseProp={}/>*/}
			{/*<Row className="justify-content-md-center">
			{courses}
			</Row>*/}
			{courses}
		</>
		)
}

