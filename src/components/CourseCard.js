import { useState, useEffect } from 'react'
import { Card, Button, Col } from 'react-bootstrap'
import {Link} from 'react-router-dom'
//import Image from "react-bootstrap/Image";

export default function CourseCard ({courseProp}) {
	//before using props, destructure the object

	const {name, description, price, _id/*, remarks*/} = courseProp
	console.log(courseProp)

	/*const [image, setImage] = useState(remarks)

	const [style, setStyle] =useState({

		width: "10rem",
		height: "20rem"
	})*/

	return (
		<Col lg={3}>
		<Card >
		<Card.Body /*style={color}*/>
			<Card.Title>
			{name}
			</Card.Title>
			<Card.Subtitle>
			Description:
			</Card.Subtitle>
			<Card.Text>
			{description}
			</Card.Text>
			<Card.Subtitle>
			Price:
			</Card.Subtitle>
			<Card.Text>
			{price}
			</Card.Text>
			{/*<Image src={image} style={style}/>*/}
			<Button variant="primary" as= {Link} to={`/courses/${_id}`}>See Details</Button>
		</Card.Body>
		</Card>
		</Col>

		
		)
}